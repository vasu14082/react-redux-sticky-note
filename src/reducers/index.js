import { combineReducers, createStore, applyMiddleware } from 'redux'
// import todos from './todos'
// import visibilityFilter from './visibilityFilter';
import notes from './notes.js';
import opennotes from './opennotes.js';
import thunk from 'redux-thunk'
// export default combineReducers({
//   todos,
//   visibilityFilter,
//   notes,
// })

let initState = {};
const persistedState = localStorage.getItem('reduxState');
if (persistedState) {
  initState = JSON.parse(persistedState)
}

const rootReducer = createStore(
  combineReducers({
    notes,
  	opennotes
  }),
  initState,
  applyMiddleware(thunk)
);

rootReducer.subscribe(() => {
  localStorage.setItem('reduxState', JSON.stringify(rootReducer.getState()))
})

// export default combineReducers({
//   	notes,
//   	opennotes
// })

export default rootReducer