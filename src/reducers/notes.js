  const notes = (state = [], action) => {
    var new_state;
  switch (action.type) {
    case 'ADD_NOTE':
      return [
        ...state,
        {
          id: action.id,
          title: action.text,
          content:[],
        }
      ]
    case 'TOGGLE_NOTE':
      return state.filter((item) => item.id !== action.id);
      // return state; 
    case 'ADD_TOD0_NOTE':
      const index = state.findIndex(post => post.id === action.noteId);
      new_state = [...state];
      var lastObject = new_state[index].content.slice(-1).pop();
      let setId = 0;
      if( lastObject )
      {
        setId = lastObject.id;
        setId++
      }

      return [
        ...state.slice(0, index),
        {...state[index],content:[...state[index].content,{id:setId,title:action.text,complete:action.complete}]},
        ...state.slice(index + 1)
      ];
       // return[ ...state, {state[index].content:state[index].content[...state.content,{id:action.noteId,title:action.text}] }]
       // return state
    case 'ADD_TODO_COMPLETE':
      const index1 = state.findIndex(post => post.id === action.noteId);
      const index2 = state[index1].content.findIndex(post => post.id === action.id);
      new_state = [...state];
      new_state[index1].content[index2].complete = !(new_state[index1].content[index2].complete);
      return new_state;

    default:
      return state
  }
}
export default notes