const opennotes = (state = [], action) => {

  switch (action.type) {
    case 'ADD_OPEN_CHAT':
    // console.log("action",state.findIndex(x => x.noteId == action.noteId))
      if((state.findIndex(x => x.noteId === action.noteId)) !== -1)
      {
        return state;
      }
      return [
        ...state,
        {
          title: action.title,
          noteId:action.noteId,
        }
      ]
    // case 'TOGGLE_OPEN_CHAT':
      // return state.map(todo =>
      //   todo.id === action.id ? { ...todo, completed: !todo.completed } : todo
      // )
      case 'CLOSE_OPEN_CHAT':
        return state.filter((item) => item.noteId !== action.noteId);

    default:
      return state
  }
}
export default opennotes