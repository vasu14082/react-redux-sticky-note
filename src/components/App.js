import React from 'react'
// import Footer from './Footer'
// import AddTodo from '../containers/AddTodo'
// import VisibleTodoList from '../containers/VisibleTodoList';
import AddNote from '../containers/AddNote';
import NoteList from './NoteList.js';
import OpenNotes from './OpenNotes.js'
// const App = () => (
//   <div>
//     <AddTodo />
//     <VisibleTodoList />
//     <Footer />
//   </div>
// )

const App = () => (
  <div>
  	<AddNote />
    <NoteList />
    <OpenNotes />
  </div>
)

export default App