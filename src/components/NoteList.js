import React from 'react'
// import PropTypes from 'prop-types'
import Note from './Note';
import { addOpenTodo } from '../actions/index.js';
// import { addNote, toggleNote} from '../actions';
import { connect } from 'react-redux';
const NoteList = (props) =>  {
  // console.log("notelist",props);
  return (
    <div>
      <h1>NOTES LIST</h1>

      <ul>
        {props.notes.map(note => (
          <Note key={note.id} {...note} onClick={() => props.dispatch(addOpenTodo(note.id,note.title))} dispatch={props.dispatch} />
        ))}
      </ul>
    </div>
  )
}

const mapStateToProps = (state) => {
  return state;
}
// const mapDispatchToProps = dispatch => ({
//   toggleNote: id => dispatch(toggleNote(id))
// })
export default connect(
  mapStateToProps,
  // mapDispatchToProps
)(NoteList)
// export default NoteList