import React from 'react'
import { connect } from 'react-redux';
import OpenNote from './OpenNote';
// import {  } from '../actions';

const OpenNotes = (props) => {
  // console.log("OpenNotes",props);
  return (
    <div>
    	{props.opennotes.length !== 0 ? (props.opennotes.map(note => (
    		<OpenNote note={note} key={note.noteId} notes={props.notes} dispatch={props.dispatch} />
    	))):(<p></p>)}
    </div>
  )
}
// Note.propTypes = {
//   onClick: PropTypes.func.isRequired,
//   completed: PropTypes.bool.isRequired,
//   text: PropTypes.string.isRequired
// }

const mapStateToProps = (state) => {
  return state
}

export default connect(mapStateToProps)(OpenNotes)