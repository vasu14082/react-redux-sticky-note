import React from 'react';
// import { connect } from 'react-redux';
import { addTodoNote, closeOpenTodo, addTodoComplete } from '../actions';

const OpenNote = (props) => {
  // console.log("OpenNote", props)
  var content = props.notes.find(x => x.id === props.note.noteId).content;
  let input
  return (
    <div>
    	 <h1>{props.note.title}</h1><p onClick={() => props.dispatch(closeOpenTodo(props.note.noteId))}>close</p>
    	 <ul>
    	 	{content.map((todo,index) => (<li onClick={() => {props.dispatch(addTodoComplete(todo.id, props.note.noteId))}} key={todo.id} style={todo.complete === true ? {color:'red'} : {color:'black'}}>{todo.title}</li>))}
    	 </ul>
    	  <form
	        onSubmit={e => {
	          e.preventDefault()
	          if (!input.value.trim()) {
	            return
	          }
	          props.dispatch(addTodoNote(props.note.noteId, props.note.title, input.value))
	          input.value = ''
	        }}
	      >
	        todo:<input ref={node => (input = node)} />
	        <button type="submit">Add ToDo</button>
	      </form>
    </div>
  )
}
// Note.propTypes = {
//   onClick: PropTypes.func.isRequired,
//   completed: PropTypes.bool.isRequired,
//   text: PropTypes.string.isRequired
// }

export default OpenNote;