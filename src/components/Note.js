import React from 'react'
// import PropTypes from 'prop-types'
// import { connect } from 'react-redux';
// import TodoList from './TodoList.js';
import { toggleNote, closeOpenTodo, toggleNoteWithId } from '../actions';

const Note = (props) => {
  // console.log("note",props)
  return (
    <p>
    <li
    key={props.id}
    onClick={props.onClick}
    >
    {props.title}
    &nbsp;&nbsp;&nbsp;&nbsp;
    </li>
    <span onClick={()=>props.dispatch(toggleNoteWithId(props.id))}>close</span>
    </p>
  )
}
// Note.propTypes = {
//   onClick: PropTypes.func.isRequired,
//   completed: PropTypes.bool.isRequired,
//   text: PropTypes.string.isRequired
// }

export default Note