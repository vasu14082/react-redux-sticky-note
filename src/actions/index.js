// let nextTodoId = 0
let nextNoteId = 0;
// export const addTodo = text => ({
//   type: 'ADD_TODO',
//   id: nextTodoId++,
//   text
// })
// export const setVisibilityFilter = filter => ({
//   type: 'SET_VISIBILITY_FILTER',
//   filter
// })
export const toggleTodo = (id, noteId )=> ({
  type: 'TOGGLE_TODO',
  id,
  noteId
})
// export const VisibilityFilters = {
//   SHOW_ALL: 'SHOW_ALL',
//   SHOW_COMPLETED: 'SHOW_COMPLETED',
//   SHOW_ACTIVE: 'SHOW_ACTIVE'
// }

export const addNote = text => ({
  type: 'ADD_NOTE',
  id: nextNoteId++,
  text
})

export const toggleNote = id => ({
  type: 'TOGGLE_NOTE',
  id
});

export const addTodoNote = (id, title,text) => ({
  type: 'ADD_TOD0_NOTE',
  noteId:id,
  title:title,
  text,
  complete:false,
});

export const addOpenTodo = (id,title) => ({
  type: 'ADD_OPEN_CHAT',
  noteId:id,
  title:title,
})

export const closeOpenTodo = (id) => ({
  type: 'CLOSE_OPEN_CHAT',
  noteId:id,
})

export const addTodoComplete = (id, noteId) => ({
  type:'ADD_TODO_COMPLETE',
  id:id,
  noteId:noteId,
  complete:true
});

// export const toggleTodo = id => ({
//   type: 'TOGGLE_TODO',
//   id
// });

export const toggleNoteWithId = (id) => {
  // return {
  //  type:"CHANGE_NAME",
  //  payload:name,
  // }
  return(dispatch) => {
    dispatch(closeOpenTodo(id));
    dispatch(toggleNote(id));
  }
}

// export default changeName;